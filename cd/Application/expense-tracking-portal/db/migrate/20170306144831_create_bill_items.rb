class CreateBillItems < ActiveRecord::Migration[5.0]
  def change
    create_table :bill_items do |t|
      t.references :bill, foreign_key: true, null: false, index: true
      t.references :category, foreign_key: true, index: true
      t.integer :state, null: false, default: 0
      t.string :full_name
      t.float :quantity
      t.float :price
      t.float :sum

      t.timestamps
    end
  end
end
