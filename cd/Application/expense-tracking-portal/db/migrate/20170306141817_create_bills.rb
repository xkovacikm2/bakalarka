class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :shop, foreign_key: true
      t.date :date, index: true
      t.time :time
      t.string :dkp
      t.float :sum, null: false
      t.boolean :registered, null: false, default: false

      t.timestamps
    end
  end
end
