# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170306144831) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bill_items", force: :cascade do |t|
    t.integer  "bill_id",                 null: false
    t.integer  "category_id"
    t.integer  "state",       default: 0, null: false
    t.string   "full_name"
    t.float    "quantity"
    t.float    "price"
    t.float    "sum"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["bill_id"], name: "index_bill_items_on_bill_id", using: :btree
    t.index ["category_id"], name: "index_bill_items_on_category_id", using: :btree
  end

  create_table "bills", force: :cascade do |t|
    t.integer  "user_id",                    null: false
    t.integer  "shop_id"
    t.date     "date"
    t.time     "time"
    t.string   "dkp"
    t.float    "sum",                        null: false
    t.boolean  "registered", default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["date"], name: "index_bills_on_date", using: :btree
    t.index ["shop_id"], name: "index_bills_on_shop_id", using: :btree
    t.index ["user_id"], name: "index_bills_on_user_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string  "name"
    t.integer "parent_id"
    t.integer "lft",                        null: false
    t.integer "rgt",                        null: false
    t.integer "depth",          default: 0, null: false
    t.integer "children_count", default: 0, null: false
    t.index ["lft"], name: "index_categories_on_lft", using: :btree
    t.index ["parent_id"], name: "index_categories_on_parent_id", using: :btree
    t.index ["rgt"], name: "index_categories_on_rgt", using: :btree
  end

  create_table "shops", force: :cascade do |t|
    t.string "name"
    t.string "address"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "name",                   default: "",     null: false
    t.date     "birthday",                                null: false
    t.string   "gender",                 default: "male", null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "bill_items", "bills"
  add_foreign_key "bill_items", "categories"
  add_foreign_key "bills", "shops"
  add_foreign_key "bills", "users"
end
