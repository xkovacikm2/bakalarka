def load_categories(category, parent, arr)
  unless category.nil?
    category.each do |key, value|
      cat = Category.create(name: key, parent: parent)
      arr << cat
      load_categories value, cat, arr
    end
  end
end

categories = []
cat_yaml = YAML::load_file Rails.root.join('db', 'seeds', 'categories.en.yml')
load_categories cat_yaml, nil, categories

User.create name: 'kovko', email: 'kovko@kovko.sk', password: '123456', password_confirmation: '123456',
            birthday: Date.yesterday

5.times do |i|
 Shop.create name: Faker::Company.name, address: "Bratislava, Ilkovicova #{i+1}"
end

500.times do |i|
 bill = User.first.bills.create shop_id: rand(3)+1, date: (Date.today - i), time: Time.now,
     dkp: '9002020845332004', sum: 100
 10.times do
   bill.bill_items.create full_name: Faker::Commerce.product_name, quantity: 2, price: 2.5, sum: 5,
     state: :set, category: categories[rand categories.size]
 end
end
