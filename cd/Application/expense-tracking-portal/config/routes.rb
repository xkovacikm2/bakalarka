Rails.application.routes.draw do

  devise_for :users, controllers: {
      sessions: 'user/sessions',
      registrations: 'user/registrations',
      passwords: 'user/passwords'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'change_locale' => 'i18n#change_locale'

  root 'site#categories'
  get 'bills' => 'site#bills'
  get 'bills/:id' => 'site#bills'

  namespace :api do
    namespace :v1 do
      get 'categories/all_select' => 'categories#all_select'
      get 'categories/show_children/:id' => 'categories#show_children'
      get 'categories/show_items/:id' => 'categories#show_items'
      post 'ocr/post_json' => 'ocr#post_json'
      post 'user/get_user' => 'user#get_user'

      resources :categories, only: [:index]
      resources :bills, only: [:index, :create, :update, :destroy, :show]
      resources :bill_items, only: [:create, :update, :destroy]
    end
  end

end
