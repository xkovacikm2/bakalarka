class Category < ApplicationRecord
  acts_as_nested_set dependent: :destroy
  has_many :bill_items

  scope :from_date, -> (from_date) do
    joins(:bill_items)
        .joins('bills on bills.id = bill_items.bill_id')
        .where('bills.date >= ?', from_date)
  end
  scope :to_date, -> (to_date) do
    joins(:bill_items)
        .joins('bills on bills.id = bill_items.bill_id')
        .where('bills.date <= ?', to_date)
  end

  def table_format(filter_dates, user)
    {
        id: self.id,
        name: self.name,
        sum_now: BillItem.from_date(filter_dates[:from]).to_date(filter_dates[:to]).
            user(user).belongs_to_children_of(self).sum(:sum),
        sum_then: BillItem.from_date(filter_dates[:past_from]).to_date(filter_dates[:past_to]).
            user(user).belongs_to_children_of(self).sum(:sum),
        has_children: (self.rgt-self.lft > 1)
    }
  end
end
