class Bill < ApplicationRecord
  belongs_to :user
  belongs_to :shop
  has_many :bill_items, dependent: :destroy

  scope :from_date, -> (from) {where "bills.date >= ?", from}
  scope :to_date, -> (to) {where "bills.date <= ?", to}

  default_scope {order created_at: :desc}

  before_create do
    self.sum = 0
  end

  before_save do
    self.sum = self.bill_items.inject(0){|sum, item| sum + item.sum}
  end

  def start_time
    self.date
  end

end
