class BillItem < ApplicationRecord
  belongs_to :bill
  belongs_to :category

  validates :sum, numericality: true
  validates :price, numericality: true
  validates :quantity, numericality: true

  enum state: [:uncategorized, :suggested, :set]

  scope :user, -> (user) do
    if user.respond_to? :id
      id = user.id
    else
      id = user
    end
    joins(:bill).where 'bills.user_id = ?', id
  end
  scope :from_date, -> (from) {joins(:bill).where 'bills.date >= ?', from}
  scope :to_date, -> (to) {joins(:bill).where 'bills.date <= ?', to}
  scope :belongs_to_children_of, -> (category) do
    joins(:category).where('categories.lft >= ?', category.lft).where('categories.rgt <= ?', category.rgt)
  end

  before_create do
    self.state = :uncategorized
  end

  #automatically assigns itself a category
  before_save do
    if self.uncategorized? or self.category_id == 0 or self.category_id == nil or self.category_id == ''
      categorize
    end
  end

  after_save do
    self.bill.update sum: 0
  end

  after_destroy do
    self.bill.update sum: 0
  end

  def date
    self.bill.date
  end

  def bill_table_format
    {
        id: self.id,
        full_name: self.full_name,
        quantity: self.quantity,
        price: self.price,
        sum: self.sum,
        category: self.category&.name || '',
        category_id: self.category&.id || 0,
    }
  end

  def category_table_format
    {
        id: self.id,
        full_name: self.full_name,
        quantity: self.quantity,
        price: self.price,
        sum: self.sum,
        date: self.date,
        shop: self.bill.shop.name,
        bill_id: self.bill_id
    }
  end

  private
  def categorize
    example = BillItem.where(full_name: self.full_name)&.user(self.bill.user)&.set&.last&.category
    set_category :set, example.id and return unless example.nil?
    example = BillItem.where(full_name: self.full_name)&.set&.last&.category
    set_category :suggested, example.id and return unless example.nil?
    set_category :uncategorized, Category.last
  end

  def set_category(state, category=nil)
    self.state = state
    self.category_id = category
  end

end
