module CategoriesHelper
  def include_past_date_filter
    date_filter
    diff = @filter_dates[:to] - @filter_dates[:from]
    @filter_dates[:past_from] = @filter_dates[:from] - diff
    @filter_dates[:past_to] = @filter_dates[:to] - diff
  end
end
