module BillsHelper
  def users_bill?
    @bill = Bill.find params[:id]
    redirect_unauthorized 'Cannot delete bill' unless current_user.bills.include? @bill
  end
end
