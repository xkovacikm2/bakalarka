module BillItemsHelper
  def users_bill_item?
    @bill_item = BillItem.find_by id: params[:id]
    redirect_unauthorized 'not yours' unless @bill_item.bill.user == current_user
  end
end
