module ApplicationHelper
  def redirect_unauthorized(message)
    flash[:danger] = message
    redirect_to root_path
  end

  def date_filter
    @filter_dates = {}
    @filter_dates[:from] = (params[:date_from]&.to_time || 1.month.ago).to_date
    @filter_dates[:to] = (params[:date_to]&.to_time || 0.days.ago).to_date
  end
end
