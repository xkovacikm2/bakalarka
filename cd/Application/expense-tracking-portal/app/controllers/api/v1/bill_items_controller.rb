class Api::V1::BillItemsController < Api::V1::ApiController
  before_action :authenticate_user!
  before_action :users_bill_item?, only: [:update, :destroy]

  def create
    bill = Bill.find_by id: params[:bill_id]
    item = bill.bill_items.new bill_item_params
    if item.save
      render json: item
    else
      render json: item.errors, status: :unprocessable_entity
    end
  end

  def update
    @bill_item.state = :set
    if @bill_item.update bill_item_params
      render json: @bill_item.bill_table_format
    else
      render json: @bill_item.errors, status: :unprocessable_entity
    end
  end

  def destroy
    respond_with @bill_item.destroy
  end

  private
  def bill_item_params
    params.require(:bill_item).permit :sum, :quantity, :price, :full_name, :category_id, :state
  end

end
