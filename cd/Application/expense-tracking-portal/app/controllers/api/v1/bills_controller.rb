class Api::V1::BillsController < Api::V1::ApiController
  before_action :authenticate_user!
  before_action :users_bill?, only: [:show, :update, :destroy, :register]
  before_action :date_filter, only: :index

  def index
    @bills = current_user.bills.from_date(@filter_dates[:from]).to_date(@filter_dates[:to]).eager_load(:shop)
    @shops = @bills.map(&:shop).uniq
    respond_with bills: @bills, shops: @shops
  end

  def create
    @shop = Shop.find_or_initialize_by shop_params
    if @shop.new_record?
      render json: @shop.errors, status: :unprocessable_entity and return unless @shop.save
    end
    @bill = current_user.bills.new bill_params
    @bill.shop = @shop
    if @bill.save
      render json: {shop: @shop, bill: @bill}
    else
      render json: @bill.errors, status: :unprocessable_entity
    end
  end

  def show
    items = @bill.bill_items.eager_load(:category).map do |item|
      item.bill_table_format
    end
    respond_with parent: {bill: @bill, shop: @bill.shop}, items: items
  end

  def update
    @shop = @bill.shop
    render json: @shop.errors, status: :unprocessable_entity and return unless @shop.update shop_params
    if @bill.update bill_params
      render json: {shop: @shop, bill: @bill}
    else
      render json: @bill.errors, status: :unprocessable_entity
    end
  end

  def destroy
    respond_with @bill.destroy
  end

  def register

  end

  private
  def bill_params
    params.require(:bill).permit :sum, :dkp, :date, :time
  end

  def shop_params
    params.require(:shop).permit :name, :address
  end
end
