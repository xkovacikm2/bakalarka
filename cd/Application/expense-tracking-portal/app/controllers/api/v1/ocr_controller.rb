class Api::V1::OcrController < Api::V1::ApiController
  def post_json
    parameters = {}
    parameters[:user_id] = params["user_id"]
    parameters[:bill] = params["bill"]
    puts parameters
    user = User.find_by id: parameters[:user_id]
    shop = Shop.find_or_create_by shop_params(parameters[:bill])
    bill = user.bills.new bill_params(parameters[:bill])
    bill.shop = shop
    bill.save
    parameters[:bill]['items'].each do |item_pars|
      bill.bill_items.create item_params(item_pars)
    end
  end

  private
  def shop_params(pars)
    puts pars
    {
        address: pars['address'],
        name: pars['shop']
    }
  end

  def bill_params(pars)
    date = Date.parse pars[:date] rescue Date.current
    time = Time.parse pars[:time] rescue Time.current
    {
        dkp: pars['vat'],
        date: date,
        time: time,
        sum: pars['sum']
    }
  end

  def item_params(pars)
    puts pars
    {
      full_name: pars['name'],
      sum: pars['sum'].to_f,
      quantity: pars['quantity'].to_f,
      price: pars['price'].to_f
    }
  end
end
