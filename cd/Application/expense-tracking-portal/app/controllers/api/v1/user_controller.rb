class Api::V1::UserController < Api::V1::ApiController
  def get_user
    user = User.find_for_authentication(email: params[:user][:email])
    if user&.valid_password? params[:user][:password]
      render json: user
    else
      raise ActionController::RoutingError, 'Not Found'
    end
  end
end
