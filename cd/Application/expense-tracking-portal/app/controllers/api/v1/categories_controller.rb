class Api::V1::CategoriesController < Api::V1::ApiController
  before_action :authenticate_user!
  before_action :include_past_date_filter, only: [:index, :show_items, :show_children]

  def index
    @categories_data = load_table_data Category.roots
    respond_with @categories_data
  end

  def show_children
    category = Category.find_by id: params[:id]
    @categories_data = load_table_data category.children
    respond_with @categories_data
  end

  def show_items
    category = Category.find_by id: params[:id]
    bill_items = BillItem.belongs_to_children_of(category).from_date(@filter_dates[:from]).to_date(@filter_dates[:to]).map do |item|
      item.category_table_format
    end
    respond_with bill_items
  end

  def all_select
    categories = Category.all.map do |category|
      {
          value: category.id,
          label: category.name
      }
    end
    respond_with categories
  end

  private
  def load_table_data(categories)
    puts categories
    categories.map do |category|
      category.table_format @filter_dates, current_user
    end
  end
end
