class SiteController < ApplicationController
  before_action :authenticate_user!

  def categories
  end

  def bills
  end
end
