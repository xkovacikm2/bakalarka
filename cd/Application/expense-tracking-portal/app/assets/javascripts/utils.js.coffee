@currencyFormat = (amount) -> Number(amount).toLocaleString() + ' €'

@findShopById = (id, shops) ->
  for shop in shops
    return shop if shop.id == id

@renderModal = (content) ->
   React.DOM.div null,
      React.DOM.button
        className: 'btn btn-info btn-lg btn-add'
        'data-toggle': 'modal'
        'data-target': '#bill-form-modal'
        '+'
      React.DOM.div
        id: 'bill-form-modal'
        className: 'modal fade'
        role: 'dialog'
        React.DOM.div
          className: 'modal-dialog'
          React.DOM.div
            className: 'modal-content'
            React.DOM.div
              className: 'modal-header'
              React.DOM.button
                className: 'close'
                'data-dismiss': 'modal'
                'X'
            React.DOM.div
              className: 'modal-body'
              content()
