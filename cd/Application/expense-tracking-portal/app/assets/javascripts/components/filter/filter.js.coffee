@Filter = React.createClass
  getInitialState: ->
    dateFrom: @props.data.date_from
    dateTo: @props.data.date_to

  handleSubmit: (e) ->
    e.preventDefault()
    data = {
      date_from: ReactDOM.findDOMNode(@refs.dateFrom).value
      date_to: ReactDOM.findDOMNode(@refs.dateTo).value
    }
    @props.handleChange data

  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group input-group date'
        React.DOM.input
          type: 'text'
          className: 'datetimepicker-date form-control'
          placeholder: 'Date from:'
          ref: 'dateFrom'
          defaultValue: @state.dateFrom
      React.DOM.div
        className: 'form-group input-group date'
        React.DOM.input
          type: 'text'
          className: 'datetimepicker-date form-control'
          placeholder: 'Date to:'
          ref: 'dateTo'
          defaultValue: @state.dateTo
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        'Filter'