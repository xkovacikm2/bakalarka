@Main = React.createClass
  getInitialState: ->
    page: @props.page
    filter:
      date_from: moment().subtract(30, 'days').format(dateFormat())
      date_to: moment().format(dateFormat())

  changeFilter: (filter) ->
    console.log filter
    @replaceState
      filter: filter
      page: @props.page

  render: ->
    React.DOM.div null,
      React.createElement Filter
      , handleChange: @changeFilter
      , data: @state.filter
      if @state.page == 'categories'
        React.createElement CategoriesTable
        , params: @props.params
        , filter: @state.filter
      else if @state.page == 'bills'
        React.createElement BillsTable
        , params: @props.params
        , filter: @state.filter
