@BillsTableRow = React.createClass
  getInitialState: ->
    editable: false

  handleClick: (e) ->
    unless @state.editable
      @props.handleClickRow @props.bill

  handleToggle: (e) ->
    e.preventDefault()
    if @props.handleEditRow?
      @setState editable: !@state.editable

  handleDelete: (e) ->
    e.preventDefault()
    if @props.handleDeleteRow?
      $.ajax
        method: 'DELETE'
        url: "/api/v1/bills/#{@props.bill.id}"
        datatype: 'JSON'
        success: () =>
          @props.handleDeleteRow @props.bill

  handleEdit: (e) ->
    e.preventDefault()
    data = {
      shop:
        name: ReactDOM.findDOMNode(@refs.name).value
        address: ReactDOM.findDOMNode(@refs.address).value
      bill:
        date: ReactDOM.findDOMNode(@refs.date).value
        dkp: ReactDOM.findDOMNode(@refs.dkp).value
        time: ReactDOM.findDOMNode(@refs.time).value
    }
    $.ajax
      method: 'PUT'
      url: "/api/v1/bills/#{@props.bill.id}"
      dataType: 'JSON'
      data: data
      success: (retVal) =>
        @setState editable: false
        @props.handleEditRow retVal, @props.bill, @props.shop

  renderImmutable: ->
    React.DOM.tr
      className: 'clickable-row'
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        @props.shop.name
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        @props.shop.address
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        @props.bill.dkp
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        new moment(@props.bill.date).format(dateFormat())
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        new moment(@props.bill.time).format(timeFormat())
      React.DOM.td
        className: 'clickable'
        onClick: @handleClick
        currencyFormat(@props.bill.sum)
      React.DOM.td null,
        React.DOM.a
          onClick: @handleToggle
          React.createElement Img
          , src: '/assets/edit.png'
          , alt: 'edit button'
          , className: 'table-icon'
      React.DOM.td null,
        React.DOM.a
          onClick: @handleDelete
          React.createElement Img
          , src: '/assets/delete.png'
          , alt: 'delete button'
          , className: 'table-icon'

  renderMutable: ->
    React.DOM.tr null,
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.shop.name
          ref: 'name'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.shop.address
          ref: 'address'
      React.DOM.td null,
        React.DOM.input
          className: 'form-control'
          type: 'text'
          defaultValue: @props.bill.dkp
          ref: 'dkp'
      React.DOM.td null,
        React.DOM.div
          className: 'form-group input-group date'
          React.DOM.input
            type: 'text'
            className: 'datetimepicker-date form-control'
            ref: 'date'
            defaultValue: new moment(@props.bill.date).format(dateFormat())
      React.DOM.td null,
        React.DOM.div
          className: 'form-group input-group date'
          React.DOM.input
            type: 'text'
            className: 'datetimepicker-time form-control'
            ref: 'time'
            defaultValue: new moment(@props.bill.time).format(timeFormat())
      React.DOM.td null,
        @props.bill.sum
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          'Update'
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'

  render: ->
    if @state.editable
      @renderMutable()
    else
      @renderImmutable()
