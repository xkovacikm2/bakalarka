@BillForm = React.createClass
  getInitialState: ->
    name: ''
    address: ''
    dkp: ''
    date: moment().format(dateFormat())
    time: moment().format(timeFormat())

  handleChange: (e) ->
    @setState "#{ e.target.name }": e.target.value

  valid: ->
    @state.name && @state.address && @state.dkp

  handleSubmit: (e) ->
    e.preventDefault()
    data = {
      bill:
        date: ReactDOM.findDOMNode(@refs.date).value
        time: ReactDOM.findDOMNode(@refs.time).value
        dkp: @state.dkp
      shop:
        name: @state.name
        address: @state.address
    }
    $.post '/api/v1/bills', data, (rowVals) =>
      @props.handleNewRecord rowVals
      @setState @getInitialState()
    , 'JSON'

  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Obchod s.r.o.'
          name: 'name'
          value: @state.name
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Mesto, Ulica číslo'
          name: 'address'
          value: @state.address
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'DKP'
          name: 'dkp'
          value: @state.dkp
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group date'
        React.DOM.input
          type: 'text'
          className: 'datetimepicker-date form-control'
          placeholder: 'Dátum'
          ref: 'date'
          defaultValue: @state.date
      React.DOM.div
        className: 'form-group input-group date'
        React.DOM.input
          type: 'text'
          className: 'datetimepicker-time form-control'
          placeholder: 'Čas'
          ref: 'time'
          defaultValue: @state.time
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        disabled: !@valid()
        'Pridať'
