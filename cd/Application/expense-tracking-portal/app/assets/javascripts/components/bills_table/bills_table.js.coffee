@BillsTable = React.createClass
  getInitialState: ->
    bills: null
    shops: null
    previous: null
    parent: null
    items: null
    filter: @props.filter

  componentDidMount: ->
    @reloadData()

  componentDidUpdate: ->
    unless @props.filter.date_from == @state.filter.date_from and @props.filter.date_to == @state.filter.date_to
      @reloadData()

  reloadData: ->
    if @props.params? and @props.params.id?
      @loadSingleBill @props.params.id
    else
      @loadBills 'bills.json', @props.filter


  loadBills: (url, params=null, row=null) ->
    $.get "/api/v1/#{url}", params, (data) =>
      @setState
        bills: data.bills
        shops: data.shops
        previous: @state
        parent: null
        items: null
        filter: @props.filter
    , 'JSON'

  loadSingleBill: (id) ->
    $.get "/api/v1/bills/#{id}.json", null, (data) =>
        @setState
          bills: null
          shops: null
          previous: @state
          parent: data.parent
          items: data.items
          filter: @props.filter
      , 'JSON'

  navigateUp: (bill) ->
    if @state.previous? and @state.previous.bills?
      @replaceState @state.previous
    else
      @loadBills 'bills.json', @props.filter

  navigateDown: (bill) ->
    @loadSingleBill bill.id

  addBill: (data) ->
    @setState
      bills: React.addons.update(@state.bills, {$push: [data.bill]})
      shops: React.addons.update(@state.shops, {$push: [data.shop]})

  removeBill: (bill) ->
    @setState bills: React.addons.update(@state.bills, {$splice: [[@state.bills.indexOf(bill), 1]]})

  editBill: (row, bill, shop) ->
    @setState
      bills: React.addons.update(@state.bills, {$splice: [[@state.bills.indexOf(bill), 1, row.bill]]})
      shops: React.addons.update(@state.shops, {$splice: [[@state.shops.indexOf(shop), 1, row.shop]]})

  renderForm: ->
    React.createElement BillForm, handleNewRecord: @addBill

  render: ->
    React.DOM.div
      className: 'bills-table-container'
      renderModal(@renderForm) unless @state.parent?
      React.DOM.hr null
      React.DOM.table
        className: 'bill-table'
        React.DOM.thead null, #null means no attributes
          React.DOM.tr null,
            React.DOM.th null,
              'Shop name'
            React.DOM.th null,
              'Shop address'
            React.DOM.th null,
              'VAT'
            React.DOM.th null,
              'Date'
            React.DOM.th null,
              'Time'
            React.DOM.th null,
              'Sum'
            React.DOM.th null,
              'Edit'
            React.DOM.th null,
              'Delete'
        React.DOM.tbody null,
          if @state.parent?
            React.createElement BillsTableRow
            , key: @state.parent.bill.id
            , bill: @state.parent.bill
            , shop: @state.parent.shop
            , handleClickRow: @navigateUp
          if @state.items?
            React.DOM.tr null,
              React.DOM.td
                colSpan: '100%'
                React.createElement ItemsTable
                , items: @state.items
                , mode: 'bills'
                , parent_id: @state.parent.bill.id
          if @state.bills?
            for bill in @state.bills
              React.createElement BillsTableRow
              , key: bill.id
              , bill: bill
              , shop: findShopById(bill.shop_id, @state.shops)
              , handleDeleteRow: @removeBill
              , handleEditRow: @editBill
              , handleClickRow: @navigateDown
