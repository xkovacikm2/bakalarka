@AmountPanel = React.createClass
  getInitialState: ->
    line_len: 3
    difference: @props.data.amount_now - @props.data.amount_then

  render: ->
    React.DOM.div
      className: 'row'
      React.createElement AmountBox
      , line_len: @state.line_len
      , amount: @props.data.amount_now
      , type: 'success'
      , text: 'This season'
      React.createElement AmountBox
      , line_len: @state.line_len
      , amount: @props.data.amount_then
      , type: 'info'
      , text: 'Last season'
      React.createElement AmountBox
      , line_len: @state.line_len
      , amount: @state.difference
      , type: 'warning'
      , text: 'Difference'
