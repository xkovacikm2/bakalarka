@AmountBox = React.createClass
  render: ->
    React.DOM.div
      className: "col-md-#{12/@props.line_len}"
      React.DOM.div
        className: "panel panel-#{@props.type}"
        React.DOM.div
          className: "panel-heading"
          @props.text
        React.DOM.div
          className: 'panel-body'
          currencyFormat @props.amount