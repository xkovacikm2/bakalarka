@CategoriesTable = React.createClass
  getInitialState: ->
    categories: []
    previous_state: null
    parent: null
    items: null
    filter: @props.filter

  componentDidMount: ->
    @loadCategories 'categories', @props.filter

  componentDidUpdate: ->
    unless @props.filter.date_from == @state.filter.date_from and @props.filter.date_to == @state.filter.date_to
      @loadCategories 'categories', @props.filter

  loadCategories: (url, params = null, row = null)->
    $.get "/api/v1/#{url}", params, (data) =>
      @replaceState
        previous_state: @state
        categories: data
        parent: row
        items: null
        filter: @props.filter
    , 'JSON'

  loadItems: (url, params = null, row = null)->
    $.get "/api/v1/categories/show_items/#{url}", params, (data) =>
      @replaceState
        previous_state: @state
        categories: []
        parent: row
        items: data
        filter: @props.filter
    , 'JSON'

  navigateDown: (row) ->
    if row.has_children
      @loadCategories "categories/show_children/#{row.id}.json", @props.filter, row

  showItems: (row) ->
    @loadItems "#{row.id}.json", @props.filter, row

  navigateUp: (data) ->
    @replaceState @state.previous_state

  prepLineChartData: ->
    start = moment(@props.filter.date_from, dateFormat())
    end = moment(@props.filter.date_to, dateFormat())
    diff = (end-start)/10
    result = []
    for i in [1..10]
      sum = @state.items.filter((elem)->
        date = new moment(elem.date)
        date >= (start+ (i-1)*diff) and date < (start + i*diff)
      ).reduce((sum, elem)->
        sum+=elem.sum
      ,0)
      result.push [new moment(start+i*diff).format(dateFormat()), sum]
    result

  renderPie: ->
    React.DOM.div
      className: 'col-md-4 col-xs-12 col-sm-12'
      React.createElement PieChart
      , key: @state.categories[0].id
      , name: 'Categories'
      , legend: 'Spending'
      , data: @state.categories
      , options:
          title: 'Spending in categories'

  renderLine: ->
    React.DOM.div
      className: 'col-md-4 col-xs-12 col-sm-12'
      React.createElement LineChart
      , key: @state.parent.id
      , name: 'SpentFlow'
      , legend: 'Spending'
      , data: @prepLineChartData()
      , options:
          title: 'Spent in category'


  render: ->
    sum = @state.categories.reduce(
      (a,b) -> a+b.sum_now
    , 0
    )
    React.DOM.div
      className: 'categories-table-container row'
      React.DOM.table
        className: 'categories-table col-md-8 col-xs-12 col-sm-12'
        React.DOM.thead null, #null means no attributes
          React.DOM.tr null,
            React.DOM.th null,
              'Expand'
            React.DOM.th null,
              'Category name'
            React.DOM.th null,
              'Spent in season'
            React.DOM.th null,
              '% of total'
            React.DOM.th null,
              'Previous season'
            React.DOM.th null,
              'Saved'
            React.DOM.th null,
              '% of previous'
        React.DOM.tbody null,
          if @state.parent?
            React.createElement CategoriesTableRow
            , key: @state.parent.id
            , category: @state.parent
            , handleClickRow: @showItems
            , handleExpand: @navigateUp
            , expandImage: '/assets/compress.png'
            , style: 'parent-category'
            , sum: sum
          if @state.items?
            React.DOM.tr null,
              React.DOM.td
                colSpan: '100%'
                React.createElement ItemsTable
                , items: @state.items
                , mode: 'categories'
          for category in @state.categories
            React.createElement CategoriesTableRow
            , key: category.id
            , category: category
            , handleClickRow: @showItems
            , handleExpand: @navigateDown
            , expandImage: '/assets/expand.png'
            , style: 'child-category clickable-row'
            , sum: sum
      @renderPie() if @state.categories.length > 0
      @renderLine() if @state.items?


