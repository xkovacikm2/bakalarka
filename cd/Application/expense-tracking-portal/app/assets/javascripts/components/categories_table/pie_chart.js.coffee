@PieChart = React.createClass
  getInitialState: ->
    data = [[@props.name, @props.legend]]
    for item in @props.data
      data.push [item.name, item.sum_now]
    data: data

  render: ->
    React.DOM.div
      className: 'chart-container'
      React.createElement ReactGoogleCharts.default.Chart
      , chartType: 'PieChart'
      , data: @state.data
      , options: @props.options