@LineChart = React.createClass
  getInitialState: ->
    data = [['Date', 'Sum']]
    for row in @props.data
      data.push row
    data: data

  render: ->
    React.DOM.div
      className: 'chart-container'
      React.createElement ReactGoogleCharts.default.Chart
      , chartType: 'LineChart'
      , data: @state.data
      , options: @props.options
