@CategoriesTableRow = React.createClass
  handleClick: (e)->
    e.preventDefault()
    @props.handleClickRow @props.category

  handleExpand: (e)->
    e.preventDefault()
    @props.handleExpand @props.category

  renderPercentages: (percentages) ->
    React.DOM.div null,
      React.DOM.div null,
        if percentages < 100
          React.createElement Img
          , src: '/assets/arrow-green.png'
          , alt: 'green arrow up'
          , className: 'expense-icon'
        else if percentages > 100
          React.createElement Img
          , src: '/assets/arrow-red.png'
          , alt: 'red arrow down'
          , className: 'expense-icon'
        else
          React.createElement Img
          , src: '/assets/equal-aqua.png'
          , alt: 'aquamarine equal sign'
          , className: 'expense-icon'
      React.DOM.div null,
        percentages.toFixed(2) + '%'

  render: ->
    React.DOM.tr
      className: @props.style
      React.DOM.td
        onClick: @handleExpand
        React.createElement Img
        , src: @props.expandImage
        , alt: 'expand button'
        , className: 'expense-icon'
      React.DOM.td
        onClick: @handleClick
        @props.category.name
      React.DOM.td
        onClick: @handleClick
        currencyFormat @props.category.sum_now
      React.DOM.td
        onClick: @handleClick
        if @props.category.sum_now > 0 and @props.sum > 0
          (@props.category.sum_now / @props.sum * 100).toFixed(2) + '%'
        else
          "not available"
      React.DOM.td
        onClick: @handleClick
        currencyFormat @props.category.sum_then
      React.DOM.td
        onClick: @handleClick
        currencyFormat @props.category.sum_then - @props.category.sum_now
      React.DOM.td
        onClick: @handleClick
        if @props.category.sum_now > 0 and @props.category.sum_then > 0
          @renderPercentages @props.category.sum_now / @props.category.sum_then * 100
        else
          "not available"
