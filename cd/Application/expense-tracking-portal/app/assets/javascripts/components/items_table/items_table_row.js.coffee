@ItemsTableRow = React.createClass
  getInitialState: ->
    editable: false
    sum: @props.billItem.sum
    quantity: @props.billItem.quantity
    price: @props.billItem.price
    category_id: @props.billItem.category_id
    categoryOptions: null

  handleToggle: (e) ->
    e.preventDefault()
    unless @state.categoryOptions?
      $.get '/api/v1/categories/all_select', null, (categories) =>
        @setState
          categoryOptions: categories
      , 'JSON'
    @setState
      editable: !@state.editable

  handleEdit: (e) ->
    e.preventDefault()
    data = {
      bill_item:
        id: @props.billItem.id
        quantity: @state.quantity
        price: @state.price
        sum: @state.sum
        full_name: ReactDOM.findDOMNode(@refs.full_name).value
        category_id: @state.category_id
    }
    $.ajax
      method: 'PUT'
      url: "/api/v1/bill_items/#{@props.billItem.id}"
      data: data
      datatype: 'JSON'
      success: (retVal) =>
        @setState editable: false
        @props.handleEdit @props.billItem, retVal

  calculateSum: (e) ->
    other = if e.target.name == 'price' then 'quantity' else 'price'
    sum= e.target.value * @state[other]
    @setState
      sum: sum
      "#{ e.target.name }": e.target.value

  handleCategoryChange: (e) ->
    @setState category_id: e.value

  handleDelete: (e) ->
    e.preventDefault()
    $.ajax
      method: 'DELETE'
      url: "/api/v1/bill_items/#{@props.billItem.id}"
      datatype: 'JSON'
      success: (retVal) =>
        @props.handleDelete @props.billItem

  renderImmutable: ->
    React.DOM.tr
      className: 'clickable-row'
      React.DOM.td null,
        @props.billItem.full_name
      React.DOM.td null,
        @props.billItem.quantity
      React.DOM.td null,
        @props.billItem.price
      React.DOM.td null,
        @props.billItem.sum
      if @props.mode == 'categories'
        React.DOM.td null,
          @props.billItem.date
      if @props.mode == 'categories'
        React.DOM.td null,
          @props.billItem.shop
      if @props.mode == 'categories'
        React.DOM.td null,
          React.DOM.a
            className: 'btn btn-default'
            href: "/bills/#{@props.billItem.bill_id}"
            'View bill'
      if @props.mode == 'bills'
        React.DOM.td null,
          @props.billItem.category
      if @props.mode == 'bills'
        React.DOM.td null,
          React.DOM.a
            onClick: @handleToggle
            React.createElement Img
            , src: '/assets/edit.png'
            , alt: 'edit button'
            , className: 'table-icon'
      if @props.mode == 'bills'
        React.DOM.td null,
          React.DOM.a
            onClick: @handleDelete
            React.createElement Img
            , src: '/assets/delete.png'
            , alt: 'delete button'
            , className: 'table-icon'

  renderMutable: ->
    React.DOM.tr
      className: 'clickable-row'
      React.DOM.td null,
        React.DOM.input
          type: 'text'
          ref: 'full_name'
          className: 'form-control'
          defaultValue: @props.billItem.full_name
      React.DOM.td null,
        React.DOM.input
          type: 'text'
          name: 'quantity'
          className: 'form-control'
          onChange: @calculateSum
          value: @state.quantity
      React.DOM.td null,
        React.DOM.input
          type: 'text'
          name: 'price'
          className: 'form-control'
          onChange: @calculateSum
          value: @state.price
      React.DOM.td
        ref: 'sum'
        @state.sum
      React.DOM.td null,
        React.createElement Select
        , name: 'category_id'
        , value: @state.category_id
        , options: @state.categoryOptions
        , onChange: @handleCategoryChange
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-default'
          onClick: @handleEdit
          'Update'
      React.DOM.td null,
        React.DOM.a
          className: 'btn btn-danger'
          onClick: @handleToggle
          'Cancel'

  render: ->
    if @state.editable
      @renderMutable()
    else
      @renderImmutable()
