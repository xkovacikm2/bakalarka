@ItemsTable = React.createClass
  getInitialState: ->
    items: @props.items
    mode: @props.mode

  addItem: (billItem) ->
    @setState items: React.addons.update(@state.items, {$push: [billItem]})

  deleteItem: (billItem) ->
    @setState items: React.addons.update(@state.items, {$splice: [[@state.items.indexOf(billItem), 1]]})

  editItem: (oldItem, newItem) ->
    @setState items: React.addons.update(@state.items, {$splice: [[@state.items.indexOf(oldItem), 1, newItem]]})

  renderForm: ->
    React.createElement ItemForm
    , handleNewRecord: @addItem
    , bill_id: @props.parent_id

  render: ->
    React.DOM.div null,
      renderModal(@renderForm) if @props.mode == 'bills'
      React.DOM.table
        className: 'bill-items-table'
        React.DOM.thead null,
          React.DOM.tr null,
            React.DOM.th null,
              'Item name'
            React.DOM.th null,
              'Quantity'
            React.DOM.th null,
              'Price'
            React.DOM.th null,
              'Sum'
            if @props.mode == 'categories'
              React.DOM.th null,
                'Date'
            if @props.mode == 'categories'
              React.DOM.th null,
                'Shop'
            if @props.mode == 'categories'
              React.DOM.th null,
                'View bill'
            if @props.mode == 'bills'
              React.DOM.th null,
                'Category'
            if @props.mode == 'bills'
              React.DOM.th null,
                'Edit'
            if @props.mode == 'bills'
              React.DOM.th null,
                'Delete'
        React.DOM.tbody null,
          for billItem in @state.items
            React.createElement ItemsTableRow
            , billItem: billItem
            , key: billItem.id
            , mode: @props.mode
            , handleDelete: @deleteItem
            , handleEdit: @editItem
