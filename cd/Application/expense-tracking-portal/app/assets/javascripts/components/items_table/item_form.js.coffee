@ItemForm = React.createClass
  getInitialState: ->
    full_name: ''
    quantity: ''
    price: ''
    category_id: ''
    categoryOptions: []

  componentDidMount: ->
    @loadCategories()

  handleChange: (e) ->
    @setState "#{ e.target.name }": e.target.value

  handleCategoryChange: (e) ->
    @setState category_id: e.value

  valid: ->
    @state.full_name && @state.quantity && @state.price && @state.category_id

  loadCategories: ->
    $.get '/api/v1/categories/all_select', null, (categories) =>
      @setState
        categoryOptions: categories
    , 'JSON'

  handleSubmit: (e) ->
    e.preventDefault()
    data = {
      bill_id: @props.bill_id
      bill_item:
        full_name: @state.full_name
        quantity: @state.quantity
        price: @state.price
        sum: (@state.quantity * @state.price)
        category_id: @state.category_id
        state: 'set'
    }
    $.post '/api/v1/bill_items', data, (rowVals) =>
      @props.handleNewRecord rowVals
      @setState @getInitialState()
    , 'JSON'

  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'text'
          className: 'form-control'
          placeholder: 'Item name'
          name: 'full_name'
          value: @state.full_name
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'number'
          className: 'form-control'
          placeholder: 'Quantity'
          name: 'quantity'
          value: @state.quantity
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group'
        React.DOM.input
          type: 'number'
          className: 'form-control'
          placeholder: 'Price'
          name: 'price'
          value: @state.price
          onChange: @handleChange
      React.DOM.div
        className: 'form-group input-group'
        React.createElement Select
        , name: 'category'
        , value: @state.category_id
        , options: @state.categoryOptions
        , onChange: @handleCategoryChange
      React.DOM.button
        type: 'submit'
        className: 'btn btn-primary'
        disabled: !@valid()
        'Pridať'