@dateFormat = -> 'DD/MM/YYYY'
@timeFormat = -> 'HH:mm'

$(document).on('focus', '.datetimepicker-date', ->
  $(@).datetimepicker(
    format: dateFormat()
  )
)

$(document).on('focus', '.datetimepicker-time', ->
  $(@).datetimepicker(
    format: timeFormat()
  )
)
