class Hash
  def sum_grouped_models(method)
    self.modify_grouped_models{|v| v.sum(&:sum)}
  end

  def modify_grouped_models(&block)
    self.inject({}) { |h, (k, v)| h[k] = yield(v); h }
  end
end