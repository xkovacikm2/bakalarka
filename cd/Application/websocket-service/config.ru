require 'faye'
require 'sinatra'
require 'json'

set :port, 9292
set :faye_client, Faye::Client.new( 'http://0.0.0.0:9292/faye' )

post '/socket' do
  halt 400, 'channel or message missing' if params['channel'].nil? or params['message'].nil?
  channel = params['channel']
  message = {message: params['message']}.to_json

  puts params

  # Send data out to connected clients
  settings.faye_client.publish( channel, message )
  halt 200, 'ok'
end

Faye::WebSocket.load_adapter 'thin'
use Faye::RackAdapter, mount: '/faye', timeout: 600, extensions: []
run Sinatra::Application
