require 'sinatra'
require 'json'
require 'sidekiq'
require 'redis'
require 'rest-client'

set :environment, :production
set :port, 4567

redis_conn = proc {
  Redis.new(
      host: ENV['KILL_BILLS_REDIS_DB_HOST'],
      port: 6379,
      password: ENV['KILL_BILLS_REDIS_DB_PASS'],
      timeout: 5
  )
}

Sidekiq.configure_client do |config|
  config.redis = ConnectionPool.new(size: 15, &redis_conn)
end

Sidekiq.configure_server do |config|
  config.redis = ConnectionPool.new(size: 25, &redis_conn)
end

class ImageUploader
  include Sidekiq::Worker

  def perform(user_id, filepath)
    xml = handle_image filepath, user_id
    File.delete filepath
    return if xml.nil?
    bill = parse_xml xml, user_id
    return if bill.nil?
    upload_bill(user_id, bill)
    ws_post(user_id, "success", "bill uploaded")
  end
end

class PatternCreator
  include Sidekiq::Worker

  def perform(user_id, xml)
    File.open("/mnt/kbill/xmls/#{user_id}_#{Time.now.to_i}.xml", 'w') do |f|
      f.write xml
    end
    response = safe_post {RestClient.post 'http://kbill1.westeurope.cloudapp.azure.com/create_pattern', xml, {content_type: :xml}}
    if response.nil?
      ws_post(user_id, "failure", "pattern creation failed")
      return
    end
    upload_bill user_id, bill
    ws_post(user_id, "success", "bill uploaded")
  end
end

post "/image_upload" do
  halt 400, 'wrong parameters, expecting file and user_id' if params[:file].nil? or params[:user_id].nil?
  filepath= '/mnt/kbill/uploads/' + params[:file][:filename]
  File.open(filepath, 'w') do |f|
    f.write params[:file][:tempfile].read
  end
  ImageUploader.perform_async params[:user_id], filepath
  {message: 'file was uploaded', channel: "/#{params[:user_id]}"}.to_json
end

post "/send_marked" do
  #halt 400, 'wrong parameters, expecting user_id and xml as file' if params[:file].nil? or params[:user_id].nil?
  xml = params[:file][:tempfile].read
  PatternCreator.perform_async params[:user_id], xml
  {message: 'file was uploaded', channel: "/#{params[:user_id]}"}.to_json
end

post "/debug_api_call" do
  halt 400, 'wrong parameters, expecting file and user_id' if params[:file].nil? or params[:user_id].nil?
  filepath= '/mnt/kbill/uploads/' + params[:file][:filename]
  File.open(filepath, 'w') do |f|
    f.write params[:file][:tempfile].read
  end
  user_id = params[:user_id]
  xml = handle_image filepath, user_id
  ws_post(user_id, 'xml', xml)
  halt 404, 'pattern not found'
end

def handle_image(filepath, user_id)
  #send file for OCR
  begin
    ocr_result = RestClient.post 'http://kbill1.westeurope.cloudapp.azure.com/file', {'filename' => filepath}.to_json, {content_type: :json}
  rescue
    ws_post user_id, 'failure', 'ocr_failed'
    return nil
  end
  ocr_result.body
end

def parse_xml(xml, user_id)
  response = safe_post {RestClient.post 'http://kbill1.westeurope.cloudapp.azure.com/parse_xml', xml, {content_type: :xml}}
  if response.nil?
    ws_post(user_id, 'xml', xml)
    return nil
  end
  response.body
end

def upload_bill(user_id, bill)
  bill_up = JSON.parse bill
  safe_post {RestClient.post 'http://kbill1.westeurope.cloudapp.azure.com/api/v1/ocr/post_json',{user_id: user_id, bill: bill_up}, {content_type: :json}}
end

def safe_post(&block)
  begin
    response = yield
  rescue
    puts 'fail'
    response = nil
  end
  response
end

def ws_post(channel, type, message)
  message = {message_type: type, content: message}
  payload = {'channel' => "/#{channel}", 'message' => message}
  RestClient.post 'http://kbill1.westeurope.cloudapp.azure.com/socket', payload
end

