\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {slovak}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}V\IeC {\'y}ber t\IeC {\'e}my projektu}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Anal\IeC {\'y}za predch\IeC {\'a}dzaj\IeC {\'u}cich projektov}{3}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Z\IeC {\'a}ver anal\IeC {\'y}zy predch\IeC {\'a}dzaj\IeC {\'u}cich projektov}{5}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Anal\IeC {\'y}za kandid\IeC {\'a}tskych n\IeC {\'a}padov na t\IeC {\'e}mu pr\IeC {\'a}ce}{5}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}T\IeC {\'e}ma projektu}{6}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Platforma KillBills}{9}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Princ\IeC {\'\i }p sprac\IeC {\'u}vania nasn\IeC {\'\i }man\IeC {\'e}ho dokladu}{9}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Mo\IeC {\v z}n\IeC {\'e} pr\IeC {\'\i }pady pou\IeC {\v z}itia platformy}{11}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Aplik\IeC {\'a}cia na sledovanie v\IeC {\'y}davkov}{13}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Po\IeC {\v z}iadavky na webov\IeC {\'u} aplik\IeC {\'a}ciu}{13}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}N\IeC {\'a}vrh}{14}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}D\IeC {\'a}tov\IeC {\'y} model}{14}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Mo\IeC {\v z}nosti zobrazovania v\IeC {\'y}davkov}{17}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Pou\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}sk\IeC {\'y} prieskum}{18}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Vyhodnotenie prieskumu}{20}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Implement\IeC {\'a}cia aplik\IeC {\'a}cie na sledovanie v\IeC {\'y}davkov}{20}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Backend}{20}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Frontend}{22}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Testovanie s pou\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}mi}{24}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Infra\IeC {\v s}trukt\IeC {\'u}ra platformy KillBills}{27}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}V\IeC {\'y}ber pou\IeC {\v z}it\IeC {\'y}ch technol\IeC {\'o}gi\IeC {\'\i }}{27}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Moduly platformy KillBills}{29}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Vytvorenie clustera z viacer\IeC {\'y}ch serverov}{30}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Kontajneriz\IeC {\'a}cia modulov}{31}{subsection.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Spr\IeC {\'a}va kontajnerov v r\IeC {\'a}mci clustera}{31}{subsection.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3}Zdie\IeC {\v l}an\IeC {\'e} \IeC {\'u}lo\IeC {\v z}isko medzi komponentami v clustri}{32}{subsection.5.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Komunik\IeC {\'a}cia medzi modulmi}{32}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.1}Odosielanie asynchr\IeC {\'o}nnej odpovede}{33}{subsection.5.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.2}Postupnos\IeC {\v t} volan\IeC {\'\i } pri sprac\IeC {\'u}van\IeC {\'\i } sn\IeC {\'\i }mky blo\IeC {\v c}ka}{34}{subsection.5.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}ver}{37}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Aplik\IeC {\'a}cia na sledovanie v\IeC {\'y}davkov}{37}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Platforma KillBills}{37}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografia}{38}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Technick\IeC {\'a} dokument\IeC {\'a}cia}{41}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Aplik\IeC {\'a}cia na sledovanie v\IeC {\'y}davkov}{41}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Modul websocket-service}{44}{section.A.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Modul work-dispatcher}{44}{section.A.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Experiment\IeC {\'a}lny prototyp d\IeC {\'a}tovodu}{45}{appendix.B}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.1}Kon\IeC {\v s}trukcia prototypu}{45}{section.B.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.2}V\IeC {\'y}sledky experiment\IeC {\'a}lneho prototypu}{46}{section.B.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {C}\IeC {\v C}l\IeC {\'a}nok na IIT.SRC}{49}{appendix.C}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {D}ImagineCup Online kolo: N\IeC {\'a}vrh projektu}{55}{appendix.D}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {E}ImagineCup Online Fin\IeC {\'a}le: Pou\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}sk\IeC {\'y} manu\IeC {\'a}l}{65}{appendix.E}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {F}Pl\IeC {\'a}n pr\IeC {\'a}ce a jeho naplnenie}{71}{appendix.F}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {G}N\IeC {\'a}vod na spustenie}{73}{appendix.G}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {H}Elektronick\IeC {\'e} m\IeC {\'e}dium}{75}{appendix.H}
